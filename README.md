# README

# **I'm here, now what?**
In this repository you will find documentation on just about anything regarding the NAC systems. Visit the [Wiki](https://bitbucket.org/nwunetworking/nac-documentation/wiki/Home) to browse through all the different documentation. Here you will find everything from why to how.

# **Why?**
The idea is to help everyone understand why we are standardising on some aspects, how the NAC fits into the larger network deployment and to keep everyone up to date when things change. Please start off with the Wiki page. There you will find answers to a lot of the questions we assume you already have.

# **Can I help?**
Of course you can! We encourage it! A detailed document on how to contribute will be written up soon. Please be on the lookout!

# **...and if something goes wrong?**
Don't stress, we are here to help everyone. For the moment, please contact Hans.Erasmus@nwu.ac.za for any assistance.